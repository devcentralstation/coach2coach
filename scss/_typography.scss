@import 'includes/settings';

//.typography{
strong {
	font-weight: 700;
	font-size: inherit;
}

h1, h2 {
	font-family: 'Neutraface2Display bold';
	text-transform: uppercase;
	letter-spacing: 3px;
}

h1 {
	font-weight: 900;
	font-size: 24px;
	line-height: 26px;
	color: $darkblue;
	font-family: "Neutraface2Display titling";
	text-transform: uppercase;
	display: block;
	padding-bottom:12px;
	margin-top:0;
	margin-bottom: 10px;

	a {
		text-decoration: none;
		font-weight: 900;
		font-size: 34px;
		line-height: 35px;
	}
}

h2 {
	font-weight: 300;
	font-size: 21px;
	line-height: 26px;
	margin-top: 0;

	a {
		text-decoration: none;
		font-weight: 300;
		font-size: 21px;
		line-height: 26px;
	}
}

h3 {
	font-family: "Helvetica";
	font-size: 16px;
	font-weight: 700;
}

h4 {
	font-family: "Helvetica";
	font-size: 14px;
	font-weight: normal;
	margin-bottom: 4px;
}

p {
	-webkit-touch-callout: none;
	-webkit-user-select: none;
	-khtml-user-select: none;
	-moz-user-select: none;
	-ms-user-select: none;
	user-select: none;
}

.blue-box {
	font-size: 16px;
	line-height: 21px;
}

.white-text{
	color: white;
	text-align: center;
	display: block;
	text-transform: uppercase;
	text-decoration: none;
	font-family:"Neutraface2Display bold";
	letter-spacing: 2px;
}

//Pages - default styles
h1.pagetitle{
	width: 100%;
	padding: 0 15px;
	font-weight: normal;
	display: table-cell;
	vertical-align:middle;
	text-align: center;
	color: #01AEEF;
	font-family: "neutraface2display titling";
	letter-spacing: 3px;
	text-shadow: 0px 0px 30px white;
}

.scenario-list, .question-list{
	ul li {
		a{
			text-decoration: none;
			h2{
				span{
					position: absolute;
					left: 0;
				}
				text-transform: none;
				color: #04adee;
				font-weight: 700;
				letter-spacing: 0px;
				font-family: "Helvetica";
				padding: 4px 0px;
				position:relative;
				padding-left: 35px;
			}
			p{
				color:black;
				padding-left: 23px;
			}
		}
	}
}

.content-container{
	.title-section{
		h2{
			color: #04adee;
			font-weight: 700;
			text-transform: capitalize;
			letter-spacing: 0px;
			font-family: "Helvetica";
			padding: 4px 0px;
		}
		p{
			color:black;
		}
	}
	ul{
		&.hidden{
			display:none;
		}
		li{
			h3{
				color: #04adee;
				padding-left: 23px;
				position: relative;
				span{
					position: absolute;
					left: 0;
				}
			}
			h4{
				padding-left: 23px;
				margin-top: 6px;
			}
			p.hidden{
				display:none;
			}

		}
	}
}

.typography{
	h1{
		text-transform: capitalize;
		color: #04adee;
		font-weight: 700;
		font-size: 16px;
		line-height: 18px;
	}
	h2{
		text-transform: capitalize;
		color: #04adee;
		font-weight: 700;
		font-size: 14px;
		line-height: 16px;
	}
	h3{
		text-transform: capitalize;
		color: #04adee;
		font-weight: 700;
		font-size: 12px;
		line-height: 14px;
	}
	h4{
		letter-spacing: 0;
		font-family: "Helvetica";
		line-height: 12px;
		font-weight: normal;
		padding-bottom: 7px;
		text-transform: none;
	}
	p{
		font-family: "Helvetica";
		font-weight: normal;
		padding-bottom: 7px;
		text-transform: none;
	}
	ul{
		list-style-type: circle;
		margin-left: 17px;
		padding: 10px 0px;
		li{
			padding: 5px 0px;
		}
	}
	a{
		color: #04adee;
		text-decoration: underline;
	}

	&.small{
		h1{
			text-transform: capitalize;
			color: #61abeb;
			font-weight: 700;
			line-height: 20px;
			font-size: 18px;
		}
		h2{
			text-transform: capitalize;
			color: #04adee;
			font-weight: 700;
			line-height: 18px;
			font-size: 15px;
		}
		h3{
			text-transform: capitalize;
			color: #04adee;
			font-weight: 700;
			line-height: 16px;
		}
		h4{
			line-height: 16px;
			padding: 5px 0px;
		}
		ul, ol{
			margin-left: 17px;
			padding: 10px 0px;
			li{
				line-height: 16px;
				padding: 5px 0px;
				font-size: 11px;
			}
		}
		ul{
			list-style-type: circle;
		}
		ol{
			list-style-type: decimal;
		}
		a{
			color: #04adee;
			text-decoration: underline;
		}

	}
}
//}