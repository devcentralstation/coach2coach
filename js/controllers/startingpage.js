myApp.controller('StartPageController',
    function($scope, $location, $routeParams, $http, $rootScope) {
        $('#startingpage').closest('body').addClass('starting-page');

        //---------------------------------------------
        //  Check if user has selected RememberLogin
        //---------------------------------------------
        if (window.localStorage.getItem("RememberLogin") == "true"){
            var email = window.localStorage.getItem("Email");
            var appLoginKey = window.localStorage.getItem("AppLoginKey");
            var address =  $rootScope.domainAddress + "/app-api/verify/?email=" + email + "&appLoginKey=" + appLoginKey;
            $http.get(address).success(function (data) {
                if (data["status"] == 200){
                    $('#login').hide();
                    $location.path('/mainmenu');
                }
            })
        }

        //---------------------------------------------
        //  Retrieve Content Data
        //---------------------------------------------

        var address = $rootScope.domainAddress + "/app-api/content/intro-page/";
        $http.get(address).success(function(data) {
            $scope.introtext =  data[0]["introtext"];
            $scope.slides = data[0]["slides"];
            $('#load').hide();
        });

        //---------------------------------------------
        //  View handling methods
        //---------------------------------------------

        $scope.HideIntroduction = function(){
            $("#introduction").css("display","none");
            $("#startingpage .white-transparency").css("display","none");
            $("#startingpage .content-container").css("display","block");
            $('#load').hide();
        };

        //---------------------------------------------
        //  Transition handler method
        //---------------------------------------------

        var back = $routeParams.back;
        if(back){
            $("#introduction").css("display","none");
            $("#startingpage .white-transparency").css("display","none");
            $("#startingpage .content-container").css("display","block");
            $('#load').hide();
        }

    }
);
