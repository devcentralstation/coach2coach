myApp.controller('ScenarioController',
    function ($scope, $location, $http, $routeParams) {

        //---------------------------------------------
        //  Default values
        //---------------------------------------------

        $scope.domainAddress = "http://www.sigmoidcurve.com/";

        //---------------------------------------------
        //  Retrieve Content Data
        //---------------------------------------------

        var address = $scope.domainAddress + "/app-api/content/ScenariosPage/?key=" + window.localStorage.getItem("AppLoginKey");
        $http.get(address).success(function (data) {
            $scope.scenarios = data[0]["scenarios"];
            $scope.pagetitle = data[0]["pagetitle"];
            $scope.bannerimage = data[0]["bannerimage"];
            $scope.blueboxtext = data[0]["blueboxtext"];
            $("#scenarios").removeClass("scroll-bounce");
            setTimeout(function(){$("#scenarios").addClass("scroll-bounce");},500);
        }).
            error(function (data, status, headers, config) {
                $scope.showErrorScreen()
            });

        //---------------------------------------------
        //  Redirecting methods
        //---------------------------------------------

        $scope.showErrorScreen = function () {
            $location.path('/errorpage');
        };

        //---------------------------------------------
        //  Transition handler method
        //---------------------------------------------

        var back = $routeParams.back;
        if (back) {
            $("#scenarios").removeClass("fadeInRight").addClass("fadeInLeft");
        }
    });

myApp.controller('ScenarioDetailController',
    function ($scope, $location, $routeParams, $http, $sce) {

        //---------------------------------------------
        //  Default values
        //---------------------------------------------

        $scope.domainAddress = "http://www.sigmoidcurve.com/";
        $scope.scenarioID = $routeParams.scenarioID;
        $scope.index = $routeParams.index;
        $(this).scrollTop(0);

        //---------------------------------------------
        //  Retrieve Content Data
        //---------------------------------------------

        var address = $scope.domainAddress + "/app-api/content/ScenariosPage/?key=" + window.localStorage.getItem("AppLoginKey");
        $http.get(address).success(function (data) {
            $scope.scenario = data[0]["scenarios"].filter(function (entry) {
                return entry.scenarioID === $scope.scenarioID;
            })[0];
            $scope.keypoints = $scope.scenario["keypoints"];
            var youtubeURL = "https://www.youtube.com/embed/" + $scope.scenario['youtubeID'] + "?showinfo=0&showsearch=0&modestbranding=1&theme=light&fs=1";
            $scope.youtubeLink = $sce.trustAsResourceUrl(youtubeURL);
            $("#scenario").removeClass("scroll-bounce");
            setTimeout(function(){$("#scenario").addClass("scroll-bounce");},500);
        }).
            error(function (data, status, headers, config) {
                $scope.showErrorScreen()
            });

        //---------------------------------------------
        //  Redirecting methods
        //---------------------------------------------

        $scope.showErrorScreen = function () {
            $location.path('/errorpage');
        };

        //---------------------------------------------
        //  Transition handler method
        //---------------------------------------------
        $scope.$back = function () {
            window.history.back();
        };

        //---------------------------------------------
        //  Toggling methods
        //---------------------------------------------
        $scope.ToggleDescription = function (index) {
            var descriptionClass = "#keypoint-" + index + " .typography";
            if ($(descriptionClass).hasClass("hidden")) {
                $(descriptionClass).removeClass("hidden");
            } else {
                $(descriptionClass).addClass("hidden");
            }
        };

    }
);
