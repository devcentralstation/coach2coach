myApp.controller('JournalDetailController',
  function($scope, $location, $routeParams, $http, $rootScope) {

      //---------------------------------------------
      //  Default values
      //---------------------------------------------

      $scope.journal = {};
      $scope.journalID = $routeParams.journalID;
      $scope.newJournal = false;
      var newJournalString = $routeParams.newJournal;
      if (newJournalString == "true"){
          $scope.newJournal = true;
      }
      $scope.journalTitle = "";
      $scope.entry = "";

      $scope.newJournalID = "0";
      $scope.newJournalUpdated = false;

      $scope.cacheTitle = "";
      $scope.cacheEntry = "";

      $scope.hasBeenSavedBefore = false;
      $scope.inputChanged = false;

      //---------------------------------------------
      //  Retrieve Journal Data
      //---------------------------------------------


      var address = $rootScope.domainAddress + "/app-api/Journals/?key="+window.localStorage.getItem("AppLoginKey");
      if ($routeParams.journalID != 0){
          $http.get(address).success(function(data) {
              $("#load").hide();
              $scope.journal = data.filter(function(entry){
                  return entry.journalID === $scope.journalID;
              });
              $scope.entry = $scope.journal[0]["content"];
              $scope.journalTitle = $scope.journal[0]["title"];

              $scope.cacheTitle = $scope.journal[0]["content"];
              $scope.cacheEntry = $scope.journal[0]["title"];

              $scope.newJournal = false;
          }).
              error(function() {
                  $scope.showErrorScreen()
              });
      } else {
          $("#load").hide();
      }

      //---------------------------------------------
      //  Saving Journal
      //---------------------------------------------

      $scope.SaveJournal = function(){
          $("#load").show();
          var address = "";
          $scope.syncClient();
          $scope.HideSavePopup();
          if ($scope.newJournal == true){
              if ($scope.newJournalID != "0"){
                  address = $rootScope.domainAddress + "/app-api/savejournal/?key="+window.localStorage.getItem("AppLoginKey")+"&title="+encodeURIComponent($scope.journalTitle)+"&content="+encodeURIComponent($scope.entry)+"&journalID=" + $scope.newJournalID;
                  $http.get(address).success(function(data) {
                      $("#load").hide();
                      $scope.journalID = data[0]["journalID"];
                      $scope.newJournalID = data[0]["journalID"];
                      $scope.HideSavePopup();
                      $scope.ShowConfirmPopup();
                      $scope.inputChanged = false;
                  }).
                      error(function() {
                          $("#load").hide();
                          $scope.showErrorScreen()
                      });
              }else {
                  address = $rootScope.domainAddress + "/app-api/savejournal/?key="+window.localStorage.getItem("AppLoginKey")+"&title="+encodeURIComponent($scope.journalTitle)+"&content="+encodeURIComponent($scope.entry)+"&journalID=0";
                  $http.get(address).success(function(data) {
                      $("#load").hide();
                      $scope.journalID = $scope.newJournalID;
                      $scope.HideSavePopup();
                      $scope.ShowConfirmPopup();
                      $scope.newJournalUpdated = true;
                      $scope.inputChanged = false;
                      $scope.newJournalID = data[0]["journalID"];

                      $scope.syncClient();
                  }).
                      error(function() {
                          $("#load").hide();
                          $scope.showErrorScreen()
                      });
              }
          } else {
              address = $rootScope.domainAddress + "/app-api/savejournal/?key="+window.localStorage.getItem("AppLoginKey")+"&title="+encodeURIComponent($scope.journalTitle)+"&content="+encodeURIComponent($scope.entry)+"&journalID="+$scope.journalID;
              $http.get(address).success(function(data) {
                  $("#load").hide();
                  $scope.syncClient();
                  $scope.HideSavePopup();
                  $scope.ShowConfirmPopup();
                  $scope.journalID = data[0]["journalID"];
                  $scope.inputChanged = false;
                  console.log(address);
              }).
                  error(function() {
                      $("#load").hide();
                      $scope.showErrorScreen();
                  });
          }
      };


      $scope.DeleteJournal = function(){
          $("#load").show();
          var address = "";
          $scope.HideDeletePopup();

          address = $rootScope.domainAddress + "/app-api/deletejournal/?key="+window.localStorage.getItem("AppLoginKey")+"&journalID="+$scope.journalID;
          $http.get(address).success(function(data) {
              if (data[0].status == 200) {
                  $("#load").hide();
                  $scope.HideDeletePopup();
                  $scope.journalID = data[0]["journalID"];
                  $scope.inputChanged = false;

                  //var destination = $("#exitPopup").attr("data-destination");
                  $location.path('/journals').search({back: 'true'});
              }
          });
      };



      //---------------------------------------------
      //  Required methods
      //---------------------------------------------

      $scope.checkDifferentInput = function(){
          if (!$scope.newJournal){
              if ($scope.journal[0]["content"] != "undefined"){
                  var originalContent = $scope.journal[0]["content"];
                  var originalTitle = $scope.journal[0]["title"];
                  if (originalContent == $scope.entry && originalTitle == $scope.journalTitle){
                      $scope.inputChanged = false;
                  } else {
                      $scope.inputChanged = true;
                  }
              }
          } else {
              if ($scope.newJournalID != "0"){
                  if (($scope.entry == '' || $scope.entry == null) && ($scope.journalTitle == '' || $scope.journalTitle == null)){
                      $scope.inputChanged = false;
                  } else if ($scope.cacheEntry == $scope.entry && $scope.cacheTitle == $scope.journalTitle){
                      $scope.inputChanged = false;
                  } else {
                      $scope.inputChanged = true;
                  }
              } else {
                  if ($scope.journalTitle == '' && $scope.entry == '' && $scope.newJournalID == "0"){
                      $scope.inputChanged = false;
                  } else {
                      $scope.inputChanged = true;
                  }
              }
          }

          $("#load").hide();
      }
      $scope.syncClient = function(){
          if(!$scope.newJournal){
              $scope.journal[0]["title"] = $scope.journalTitle;
              $scope.journal[0]["content"] = $scope.entry;
              $scope.cacheTitle = $scope.journalTitle;
              $scope.cacheEntry = $scope.entry;
          }
          $scope.cacheTitle = $scope.journalTitle;
          $scope.cacheEntry = $scope.entry;
          $("#load").hide();
      }
      //---------------------------------------------
      //  Redirecting methods
      //---------------------------------------------

      $scope.showErrorScreen = function(){
          $location.path('/errorpage');
      };

      //---------------------------------------------
      //  Window popup handling methods
      //---------------------------------------------
      $scope.ShowConfirmPopup = function(){
          $("#confirmPopup").css({"visibility": "visible"}).addClass("fadeIn").removeClass("fadeOut");
          $("#confirmPopup .confirmPopupBox").addClass("fadeInUp").removeClass("fadeOutUp");
      };
      $scope.ShowSavePopup = function(){
          $("#savePopup").css({"visibility": "visible"}).addClass("fadeIn").removeClass("fadeOut");
          $("#savePopup .savePopupBox").addClass("fadeInUp").removeClass("fadeOutUp");
      };
      $scope.ShowDeletePopup = function(){
          $("#deletePopup").css({"visibility": "visible"}).addClass("fadeIn").removeClass("fadeOut");
          $("#deletePopup .deletePopupBox").addClass("fadeInUp").removeClass("fadeOutUp");
      };


      // Hide popup
      $scope.HideDeletePopup = function(){
          $("#deletePopup").addClass("fadeOut").removeClass("fadeIn");
          $("#deletePopup .deletePopupBox").addClass("fadeOutUp").removeClass("fadeInUp");
          setTimeout(function() {
              $("#deletePopup").css({"visibility": "hidden"});
          }, 500);
      };
      $scope.HideSavePopup = function(){
          $("#savePopup").addClass("fadeOut").removeClass("fadeIn");
          $("#savePopup .savePopupBox").addClass("fadeOutUp").removeClass("fadeInUp");
          setTimeout(function() {
              $("#savePopup").css({"visibility": "hidden"});
          }, 500);
      };
      $scope.HideConfirmPopup = function(){
          $("#confirmPopup").addClass("fadeOut").removeClass("fadeIn");
          $("#confirmPopup .confirmPopupBox").addClass("fadeOutUp").removeClass("fadeInUp");
          setTimeout(function() {
              $("#confirmPopup").css({"visibility": "hidden"});
          }, 500);
      };
      $scope.HideExitPopup = function(){
          $("#exitPopup, #deletePopup").addClass("fadeOut").removeClass("fadeIn");
          $("#exitPopup .exitPopupBox, #deletePopup .deletePopupBox").addClass("fadeOutUp").removeClass("fadeInUp");
          setTimeout(function() {
              $("#exitPopup, #deletePopup").css({"visibility": "hidden"});
          }, 500);
      };


      //$scope.ShowExitPopup = function(Destination, hasBackParameter){
      //    $scope.selected = Destination;
      //    $scope.checkDifferentInput();
      //    if ($scope.inputChanged == true){
      //        $("#exitPopup").css({"visibility": "visible"}).addClass("fadeIn").removeClass("fadeOut").attr({"data-destination":Destination, "data-back":hasBackParameter});
      //        $("#exitPopup .exitPopupBox").addClass("fadeInUp").removeClass("fadeOutUp");
      //    } else {
      //        if (hasBackParameter=="true"){
      //            $location.path(Destination).search({back: 'true'});
      //        }else {
      //            $location.path(Destination);
      //        }
      //    }
      //};
      $scope.ShowExitPopup = function(Destination, hasBackParameter){
          $scope.selected = Destination;
          $scope.SaveJournal();

          //remove focus from and inputs to close keyboard - stupid iphone 5
          $('input, textarea').blur();

          if (hasBackParameter=="true"){
              $location.path(Destination).search({back: 'true'});
          }else {
              $location.path(Destination);
          }
      };
      $scope.ExitSaveJournal = function(){
          var destination = $("#exitPopup").attr("data-destination");
          var back = $("#exitPopup").attr("data-back");
          $scope.SaveJournal();
          if (back=="true"){
              $location.path(destination).search({back: 'true'});
          }else {
              $location.path(destination);
          }
      };
      $scope.ExitCancelJournal = function(){
          var destination = $("#exitPopup").attr("data-destination");
          var back = $("#exitPopup").attr("data-back");
          if (back=="true"){
              $location.path(destination).search({back: 'true'});
          }else {
              $location.path(destination);
          }
      };
      var newHeight = $(window).height() - 233;
      $("form #content").css({"min-height": newHeight});

      //---------------------------------------------
      //  FIX:
      //  Shrink input to show keyboard on android
      //---------------------------------------------

      $("#journal #content")
          .focus(function() {
              if(getMobileOperatingSystem() == "Android"){
                  var newHeight = $(window).height()/4;
                  $("form #content").css({"min-height":newHeight})
              }
          })
          .blur(function() {
              if(getMobileOperatingSystem() == "Android"){
                  var newHeight = $(window).height() - 233;
                  $("form #content").css({"min-height":newHeight})
              }
          });

      function getMobileOperatingSystem() {
          var userAgent = navigator.userAgent || navigator.vendor || window.opera;
          if( userAgent.match( /iPad/i ) || userAgent.match( /iPhone/i ) || userAgent.match( /iPod/i ) ) {
              return 'iOS';
          } else {
              return 'Android';
          }
      }

      //---------------------------------------------
      //  Email link method
      //  Returns proper format in email
      //---------------------------------------------

      $scope.getEmailLink = function(){
          var journalTitle = encodeURIComponent($scope.journalTitle);
          var entry = encodeURIComponent($scope.entry);
          return "mailto:?subject=" + journalTitle + "&body=" + entry;
      }
});
