myApp.controller('ExploreController',
    function ($scope, $location, $http, $rootScope) {
        //---------------------------------------------
        //  Logging in as guest
        //---------------------------------------------
        $scope.GuestLogin = function () {
            $('#load').show();
            var email = encodeURIComponent('guest@coach2coach.com');
            var password = encodeURIComponent('explore');
            var address =  $rootScope.domainAddress + "/app-api/login/?email=" + email + "&password=" + password;
            $http.get(address).success(function (data, status, headers, config) {
                if (data["status"] == 200){
                    window.localStorage.setItem("AppLoginKey", data["AppLoginKey"]);
                    window.localStorage.setItem("FirstName", data["FirstName"]);
                    window.localStorage.setItem("Surname", data["Surname"]);
                    window.localStorage.setItem("Email",  data["Email"]);
                    $('#load').hide();
                    $location.path('/mainmenu');
                } else {
                    $('#load').hide();
                    $scope.message = "Service currently unavailable, please try again later";
                    $scope.DisplayError()
                }
            }).
            error(function(data, status, headers, config) {
                $scope.message = "Service currently unavailable, please try again later";
                $scope.DisplayError();
                $('#load').hide();
            });
        }

        $scope.DisplayError = function () {
            $("#login .content-container").css("display", "none");
            $("#login-error").css("display", "block");
        }
    });
