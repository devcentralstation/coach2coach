myApp.controller('FiveStepController',

//    Get the JSON for the scenarios
    function($scope, $location, $http) {

        //---------------------------------------------
        //  Default values
        //---------------------------------------------

        $scope.domainAddress = "http://www.sigmoidcurve.com/";

        //---------------------------------------------
        //  Retrieve Content Data
        //---------------------------------------------

        var address = $scope.domainAddress + "/app-api/content/5-step-game-plan/?key="+window.localStorage.getItem("AppLoginKey");
        $http.get(address).success(function(data) {
            $scope.steps = data[0]["accordians"];
            $scope.pagetitle =  data[0]["pagetitle"];
            $scope.bannerimage =  data[0]["bannerimage"];
            $scope.blueboxtext =  data[0]["blueboxtext"];
            $("#fivestep").removeClass("scroll-bounce");
            setTimeout(function(){$("#fivestep").addClass("scroll-bounce");},500);
        }).
            error(function() {
                $scope.showErrorScreen()
            });

        //---------------------------------------------
        //  Redirecting methods
        //---------------------------------------------

        $scope.showErrorScreen = function(){
            $location.path('/errorpage');
        };

        //---------------------------------------------
        //  Toggling methods
        //---------------------------------------------

        $scope.toggleMoreText = function(pointid) {
            var moreTextID = $('#'+"more-text" + pointid);

            if (moreTextID.hasClass("hidden")){
                moreTextID.removeClass("hidden");
            }else{
                moreTextID.addClass("hidden");
            }
        };
    });
