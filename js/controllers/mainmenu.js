myApp.controller('MainMenuController',
    function ($scope, $routeParams, $http, $location) {

        //---------------------------------------------
        //  Retrieve Menu Data
        //---------------------------------------------

//        var address =  $scope.domainAddress + "/app-api/content/bio/?key="+window.localStorage.getItem("AppLoginKey");
//        $http.get(address).success(function(data) {
        var menu = window.localStorage.getItem('mainmenu');

        if (!menu) {
            $http.get('data/menu.json').success(function(data) {
                window.localStorage.setItem('mainmenu', JSON.stringify(data[0]));
                $scope.title =  data[0]["title"];
                $scope.image =  data[0]["image"];
                $scope.menuItems =  data[0]["menu"];
                $("#mainmenu").removeClass("scroll-bounce");
                setTimeout(function(){$("#mainmenu").addClass("scroll-bounce");},500);
            }).
            error(function() {
                $scope.showErrorScreen()
            });
        } else {
            menu = JSON.parse(menu);
            $scope.title =  menu["title"];
            $scope.image =  menu["image"];
            $scope.menuItems =  menu["menu"];
            $("#mainmenu").removeClass("scroll-bounce");
            setTimeout(function(){$("#mainmenu").addClass("scroll-bounce");},500);
        }


        //---------------------------------------------
        //  Return angular href link
        //---------------------------------------------

        $scope.menuLink = function(pageType, urlSegment){
            if (pageType=="SinglePage"){
                return "#/content/content/" + urlSegment;
            } else if (pageType=="AccordianPage"){
                return "#/content/accordian/" + urlSegment;
            } else if (pageType=="ListPage"){
                return "#/content/list/" + urlSegment;
            } else if (pageType=="TogglePage"){
                return "#/content/toggle/" + urlSegment;
            }
        }

        //---------------------------------------------
        //  Display guides when user registers
        //---------------------------------------------

        var showGuide = $routeParams.showGuide;
        $('#mainmenu #mainpage').css("overflow-y", "visible");

        if (showGuide) {
            $('#mainmenu #guides').show();
            $('#mainmenu #mainpage').css("overflow-y", "hidden");
        } else {
            $('#mainmenu #guides').hide();
            $('#mainmenu #guides').css("z-index", "0");
            $('#mainmenu #mainpage').css("overflow-y", "visible");
        }

        $scope.hideGuides = function () {
            $('#mainmenu #guides').addClass("animated fadeOut");
            setTimeout(function () {
                $('#mainmenu #guides').hide();
                $('#mainmenu #guides').css("z-index", "0");
            }, 500);
            $('#mainmenu #mainpage').css("overflow-y", "visible");
        };

        //---------------------------------------------
        //  Dropdown menu popup
        //---------------------------------------------

        $(".menu-icon").click(function(){
            $(".blur-toggle").each(function(){
               if ($(this).hasClass("blur-off")){
                   $(this).addClass("blur-on").removeClass("blur-off")
               } else {
                   $(this).addClass("blur-off").removeClass("blur-on");
               }
            });
            $(".dropdown-list ul li a").each(function(){
                var path = $(this).attr("ng-href");
                if (path.toLowerCase().indexOf($location.path()) >= 0) {
                    $(this).addClass("selected");
                    $(this).closest("li").css("background-image", "none");
                }
            });
            $(".close-dropdown").toggle();
            $(".dropdown-list").toggle(200);
        });

        $(".close-dropdown").click(function(){
            $(".dropdown-list").hide(200);
            $(".close-dropdown").hide();

            if ($(".dropdown-list").is(":visible")){
                $(".blur-toggle").each(function(){
                    if ($(this).hasClass("blur-off")){
                        $(this).addClass("blur-on").removeClass("blur-off");
                    } else {
                        $(this).addClass("blur-off").removeClass("blur-on");
                    }
                });
            }
        });

        $(".dropdown-list").delegate( "li .selected", "click", function() {
            $(".dropdown-list").hide(200);
            $(".close-dropdown").hide();
            if ($(".dropdown-list").is(":visible")){
                $(".blur-toggle").each(function(){
                    if ($(this).hasClass("blur-off")){
                        $(this).addClass("blur-on").removeClass("blur-off");
                    } else {
                        $(this).addClass("blur-off").removeClass("blur-on");
                    }
                });
            }
        });

        //---------------------------------------------
        //  Android fix: fixing horizontal issue
        //---------------------------------------------

        $("body").css("overflow-y", "scroll");
    }
);