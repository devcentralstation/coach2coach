myApp.controller('BioController',
    function($scope, $location, $http, $rootScope) {

        //---------------------------------------------
        //  Retrieve Content Data
        //---------------------------------------------

        var address =  $rootScope.domainAddress + "/app-api/content/bio-page/?key="+window.localStorage.getItem("AppLoginKey");
        $http.get(address).success(function(data) {
            if (data[0]["status"] == "200"){
                $("#load").hide();
                $scope.bioimage =  data[0]["bioimage"];
                $scope.bioimage =  data[0]["bioimage"];
                $scope.content =  data[0]["content"];
                $("#HaroldBio").removeClass("scroll-bounce");
                setTimeout(function(){$("#HaroldBio").addClass("scroll-bounce");},500);
            }else if (data[0]["status"] == "403"){
                $("#load").hide();
                $scope.showErrorScreen();
            }
        }).
            error(function() {
                $("#load").hide();
                $scope.showErrorScreen()
        });

        //---------------------------------------------
        //  Redirecting methods
        //---------------------------------------------

        $scope.showErrorScreen = function(){
            $location.path('/errorpage');
        };

    }

);