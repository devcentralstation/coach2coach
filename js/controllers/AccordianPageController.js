myApp.controller('AccordianPageController',

//    Get the JSON for the scenarios
  function($scope, $location, $http, $rootScope, $routeParams, $sce) {

      //---------------------------------------------
      //  Default values
      //---------------------------------------------

      $scope.urlSegment = $routeParams.urlsegment;
      $scope.media = {};

      console.log($scope.urlSegment);

      if($scope.urlSegment == '5-step-game-plan'){
          var email = window.localStorage.getItem("Email");
          var appLoginKey = window.localStorage.getItem("AppLoginKey");
          var address =  $rootScope.domainAddress + "/app-api/verify/?email=" + email + "&appLoginKey=" + appLoginKey + "&page=Journals";
          $http.get(address).success(function (data) {
              if (data["status"] == 403){
                  $location.path('/denied');
              }
          })
      }

      //---------------------------------------------
      //  Retrieve Content Data
      //---------------------------------------------

      $('iframe.wistia_embed').load(function(){
         //$('iframe.wistia_embed').contents().find("head").append("<style type='text/css'>.wistia_click_to_play{width:100% !important; border:0 !important;}</style>");
          $('iframe').contents().find("img").css('border', 'none !important');
      });

      var address = $rootScope.domainAddress + "/app-api/content/" + $scope.urlSegment + "/?key="+window.localStorage.getItem("AppLoginKey");
      $http.get(address).success(function(data) {
          if (data[0]["status"] == "200"){
              $("#load").hide();
              $scope.media = data[0]["media"][0];
              $scope.points = data[0]["points"];
              $scope.pagetitle =  data[0]["pagetitle"];
              $scope.bannerimage =  $scope.media["bannerimage"];
              $scope.blueboxtext =  data[0]["blueboxtext"];
              //$scope.youtubeLink = $sce.trustAsResourceUrl("https://www.youtube.com/embed/" + $scope.media["youtubeID"] + "/?controls=2&modestbranding=1&playsinline=0");
              $scope.wistiaLink = $sce.trustAsResourceUrl("https://fast.wistia.net/embed/iframe/" + $scope.media["wistiaID"]);

              $("#whenamicoach").removeClass("scroll-bounce");
              setTimeout(function(){$("#whenamicoach").addClass("scroll-bounce");},500);
          }else if (data[0]["status"] == "403"){
              $("#load").hide();
              $scope.showErrorScreen();
          }
      }).
          error(function(data, status, headers, config) {
              $("#load").hide();
              $scope.showErrorScreen();
          });

      //---------------------------------------------
      //  Redirecting methods
      //---------------------------------------------

      $scope.showErrorScreen = function () {
          $location.path('/errorpage');
      };

      //---------------------------------------------
      //  Toggling methods
      //---------------------------------------------

      $scope.toggleMoreText = function(pointid) {
          var moreTextID = $('#'+"more-text" + pointid),
              hasBeenToggled = moreTextID.parent().hasClass("toggle"),
              itemID = moreTextID.closest('.menu-item');

          //hide all descriptions and remove all toggles to default
          $(".description ").each(function(){
              $(this).slideUp();
          });
          $(".toggle ").each(function(){
              $(this).removeClass("toggle");
          });

          if (hasBeenToggled){

              moreTextID.parent().removeClass("toggle");
              moreTextID.hide();

          } else {
              $("#load").show();

              moreTextID.parent().addClass("toggle");
              moreTextID.slideDown(function(){
                  $('html, body').animate({
                      scrollTop: itemID.offset().top - 50
                  }, 500);
              });

              //var description = window.localStorage.getItem('gameplan_' + pointid);
              var description = false;

              if(!description){
                  var ajaxurl = $rootScope.domainAddress + "/app-api/ajaxcontent/" + $scope.urlSegment + "/?key="+window.localStorage.getItem("AppLoginKey") + '&itemID=' + pointid;
                  $http({
                      method: 'GET',
                      url: ajaxurl
                  }).then(function successCallback(data) {
                      var point = data["data"]["point"],
                          description = '';

                      if(point["video"]){
                          description += point["video"];
                      }

                      if(point["description"]){
                          description += '<div class="text">' + point["description"] + '</div>';
                      }

                      $scope.description = description;
                      window.localStorage.setItem('gameplan_' + pointid, description);

                      setTimeout(function(){ $("#load").hide(); }, 1000);
                  });
              }
              else{
                  $scope.description = description;

                  setTimeout(function(){ $("#load").hide(); }, 1000);
              }
          }
      };

      $scope.hasArrow = function(id){
            var description = $("#more-text"+id);
            if (description.text() == ''){
                return "remove-arrow";
            }
      };

      $scope.isVideoType = function(){
          var media = $scope.media;
          $('.menu-item').removeClass('remove-arrow');
          if (media || media != null || media.length >0){
              return media["type"] === "Video";
          } else {
              return false;
          }
      };

      $scope.isImageType = function(){
          var media = $scope.media;
          if (media || media != null || media.length >0){
            return media["type"] === "Image";
          }
      };

  });
