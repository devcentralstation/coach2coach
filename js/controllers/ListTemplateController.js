myApp.controller('ListController',
    function($scope, $location, $http, $routeParams, $rootScope) {

        //---------------------------------------------
        //  Default values
        //---------------------------------------------

        $scope.urlSegment = $routeParams.urlsegment;
        $scope.media = {};

        //---------------------------------------------
        //  Retrieve Content Data
        //---------------------------------------------

        var address =  $rootScope.domainAddress + "/app-api/content/" + $scope.urlSegment + "/?key="+window.localStorage.getItem("AppLoginKey");
        $http.get(address).success(function(data) {
            $("#load").hide();
            if (data[0]["status"] == "200"){
                $scope.listItems = data[0]["listItems"];
                $scope.pagetitle =  data[0]["pagetitle"];
                $scope.bannerimage =  data[0]["bannerimage"];
                $scope.blueboxtext =  data[0]["blueboxtext"];
                $("#questions").removeClass("scroll-bounce");
                setTimeout(function(){$("#questions").addClass("scroll-bounce");},500);
            } else if (data[0]["status"] == "403"){
                $("#load").hide();
                $scope.showErrorScreen();
            }
        }).
        error(function(data, status, headers, config) {
            $("#load").hide();
            $scope.showErrorScreen()
        });

        //---------------------------------------------
        //  Redirecting methods
        //---------------------------------------------

        $scope.showErrorScreen = function(){
            $location.path('/errorpage');
        };

        //---------------------------------------------
        //  Transition handler method
        //---------------------------------------------

        var back = $routeParams.back;
        if (back){
            $("#questions").removeClass("fadeInRight").addClass("fadeInLeft");
        }
    });

myApp.controller('ListDetailController', function ($scope, $location, $routeParams, $http, $rootScope, $sce){

    //---------------------------------------------
    //  Check for permission
    //---------------------------------------------

    var email = window.localStorage.getItem("Email");
    var appLoginKey = window.localStorage.getItem("AppLoginKey");
    var address =  $rootScope.domainAddress + "/app-api/verify/?email=" + email + "&appLoginKey=" + appLoginKey + "&page=Journals";
    $http.get(address).success(function (data) {
        if (data["status"] == 403){
            $location.path('/deniedscenario');
        }
    })
    //---------------------------------------------
    //  Default values
    //---------------------------------------------

    $scope.urlSegment = $routeParams.urlsegment;
    $scope.listItemID = $routeParams.listItemID;
    $scope.index = $routeParams.index;

    //---------------------------------------------
    //  Retrieve Content Data
    //---------------------------------------------

    var address =  $rootScope.domainAddress + "/app-api/content/" + $scope.urlSegment + "/?key="+window.localStorage.getItem("AppLoginKey");
    $http.get(address).success(function(data) {
        if (data[0]["status"] == "200"){
            $("#load").hide();
            $scope.listItem = data[0]["listItems"].filter(function(entry){
                return entry.listItemID === $scope.listItemID;
            })[0];
            $scope.media = $scope.listItem["media"][0];
            $scope.keypoints = $scope.listItem["keypoints"];

            //$scope.youtubeLink = $sce.trustAsResourceUrl("https://www.youtube.com/embed/" + $scope.media["youtubeID"] + "/?controls=2&modestbranding=1&playsinline=0");
            $scope.wistiaLink = $sce.trustAsResourceUrl("https://fast.wistia.net/embed/iframe/" + $scope.media["wistiaID"]);
            $("#question").removeClass("scroll-bounce");
            setTimeout(function(){$("#question").addClass("scroll-bounce");},500);
        }else if (data[0]["status"] == "403"){
            $("#load").hide();
            $scope.showErrorScreen();
        }
    }).
    error(function() {
        $("#load").hide();
        $scope.showErrorScreen();
    });

    $scope.isVideoType = function(){
        var media = $scope.media;
        if (media || media != null || media.length >0){
            return media["type"] === "Video";
        } else {
            return false;
        }
    };

    $scope.isImageType = function(){
        var media = $scope.media;
        if (media ||media != null || media.length >0){
            return media["type"] === "Image";
        }
    };

    $scope.showErrorScreen = function(){
        $location.path('/errorpage');
    };

    //Back function to go back to previous page
    $scope.$back = function() {
        window.history.back();
    };

    //Move the view to start at the top
    $(this).scrollTop(0);


    $scope.ToggleDescription = function(id, index){
        var descriptionClass = $("#keypoint-"+index + " .description");
        var hasBeenToggled = descriptionClass.parent().hasClass("toggle");
        var itemID = descriptionClass.closest('.keypoint-item');

        $(".toggle").each(function(){
            $(this).removeClass("toggle");
        });
        $(".description").each(function(){
            $(this).slideUp();
        });

        if (hasBeenToggled){
            descriptionClass.parent().removeClass("toggle");
            descriptionClass.hide();
        } else {
            $("#load").show();

            descriptionClass.parent().addClass("toggle");
            descriptionClass.slideDown(function(){
                $('html, body').animate({
                    scrollTop: itemID.offset().top - 50
                }, 500);
            });

            //var description = window.localStorage.getItem('listaccordionitem_' + id);
            var description = false;

            if(!description){
                var ajaxurl = $rootScope.domainAddress + "/app-api/ajaxcontent/" + $scope.urlSegment + "/?key="+window.localStorage.getItem("AppLoginKey") + '&itemID=' + id;
                $http({
                    method: 'GET',
                    url: ajaxurl
                }).then(function successCallback(data) {
                    var point = data["data"]["point"],
                        description = '';

                    if(point["video"]){
                        description += point["video"];
                    }

                    if(point["description"]){
                        description += '<div class="text">' + point["description"] + '</div>';
                    }

                    console.log(description);

                    $scope.description = description;
                    window.localStorage.setItem('listaccordionitem_' + id, description);

                    setTimeout(function(){ $("#load").hide(); }, 1000);
                });
            }
            else{
                $scope.description = description;

                setTimeout(function(){ $("#load").hide(); }, 1000);
            }
        }
    };


});
