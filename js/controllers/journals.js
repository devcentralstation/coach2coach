myApp.controller('JournalController',
    function ($scope, $location, $http, $routeParams, $rootScope) {

        //---------------------------------------------
        //  Check for permission
        //---------------------------------------------

        var email = window.localStorage.getItem("Email");
        var appLoginKey = window.localStorage.getItem("AppLoginKey");
        var address =  $rootScope.domainAddress + "/app-api/verify/?email=" + email + "&appLoginKey=" + appLoginKey + "&page=Journals";
        $http.get(address).success(function (data) {
            if (data["status"] == 403){
                $location.path('/denied');
            }
        })

        //---------------------------------------------
        //  Retrieve Journals Data
        //---------------------------------------------

        var address = $rootScope.domainAddress + "/app-api/Journals/?key=" + window.localStorage.getItem("AppLoginKey");
        $http.get(address).success(function (data) {
            $("#load").hide();
            $("#journals").removeClass("scroll-bounce");
            $scope.journals = data;
            setTimeout(function(){$("#journals").addClass("scroll-bounce");},500);
            if (data[0]["status"] == "403"){
                $scope.showErrorScreen();
            }
        }).
            error(function () {
                $scope.showErrorScreen()
            });

        //---------------------------------------------
        //  Redirecting methods
        //---------------------------------------------

        $scope.showErrorScreen = function () {
            $location.path('/errorpage');
        }

        //---------------------------------------------
        //  Required methods
        //---------------------------------------------

        $scope.getTitle = function (title) {
            if (title == ""){
                return "No Journal Title";
            } else {
                return title;
            }
        }

        //---------------------------------------------
        //  Transition handler method
        //---------------------------------------------

        var back = $routeParams.back;
        if (back) {
            $("#journals").removeClass("fadeInRight").addClass("fadeInLeft");
        }
    }
);
