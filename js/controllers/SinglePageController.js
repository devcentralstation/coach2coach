myApp.controller('SinglePageController',
    function ($scope, $location, $http, $rootScope, $routeParams, $window) {

        //---------------------------------------------
        //  Default values
        //---------------------------------------------

        $scope.urlSegment = $routeParams.urlsegment;

        //---------------------------------------------
        //  Retrieve Content Data
        //---------------------------------------------

        var address = $rootScope.domainAddress + "/app-api/content/" + $scope.urlSegment + "/?key=" + window.localStorage.getItem("AppLoginKey");
        $http.get(address).success(function (data) {
            if (data[0]["status"] == "200"){
                $("#load").hide();
                $scope.pagetitle = data[0]["pagetitle"];
                $scope.bannerimage = data[0]["bannerimage"];
                $scope.content = data[0]["content"];
                $scope.blueboxtext = data[0]["blueboxtext"];
                $("#whatisc2c").removeClass("scroll-bounce");
                setTimeout(function(){$("#whatisc2c").addClass("scroll-bounce");},500);
            } else if (data[0]["status"] == "403"){
                $("#load").hide();
                $scope.showErrorScreen();
            }
        }).
            error(function () {
                $("#load").hide();
                $scope.showErrorScreen();
            });

        //---------------------------------------------
        //  Redirecting methods
        //---------------------------------------------

        $scope.showErrorScreen = function () {
            $location.path('/errorpage');
        };


        // Open in new window anything that has target blank.
        $(".text-content").delegate( "a[target='_blank']", "click", function(e) {
            e.preventDefault();
            var url = $(this).attr('href');
            $window.open(url, '_system', 'location=yes');
        });

    }
);