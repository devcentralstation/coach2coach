myApp.controller('TermsController',
    function($scope, $location, $http, $rootScope) {

        //---------------------------------------------
        //  Retrieve Content Data
        //---------------------------------------------

        var address = $rootScope.domainAddress + "/app-api/content/terms-and-conditions/";
        $http.get(address).success(function(data) {
            $("#load").hide();
            $scope.content =  data[0]["content"];
        });

        //---------------------------------------------
        //  Android style fixes: Container size issue
        //---------------------------------------------
        $("body").css("height", "inherit");

        if(getMobileOperatingSystem() == "iOS"){
            $("#TermsConditions .content-container").addClass("fadeInUp");
        }

        function getMobileOperatingSystem() {
            var userAgent = navigator.userAgent || navigator.vendor || window.opera;
            if( userAgent.match( /iPad/i ) || userAgent.match( /iPhone/i ) || userAgent.match( /iPod/i ) ) {
                return 'iOS';
            } else {
                return 'Android';
            }
        }
    }
);