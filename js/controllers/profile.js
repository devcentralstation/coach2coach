myApp.controller('ProfileController',
  function($scope, $timeout, $location, $routeParams, $http, $rootScope, $window) {

      //---------------------------------------------
      //  Default values
      //---------------------------------------------

      $scope.changepassword = "show";
      $scope.showoptions = "show";
      $scope.savestatus = "";

      $scope.fullName = window.localStorage.getItem("FirstName") + " " + window.localStorage.getItem("Surname");
      $scope.email =  window.localStorage.getItem("Email");
      $('#popup').hide();

      $scope.emailAddress = "";
      $scope.androidLink = "";
      $scope.iosLink = "";
      $scope.longitude = "";
      $scope.latitude = "";

      //---------------------------------------------
      //  Retrieve Content Data
      //---------------------------------------------

      var address = $rootScope.domainAddress + "/app-api/settings/?key="+window.localStorage.getItem("AppLoginKey");
      $http.get(address).success(function(data) {
          if (data[0]["status"] == "200"){
              $scope.emailAddress = data[0]["email"];
              $scope.androidLink = data[0]["androidLink"];
              $scope.iosLink = data[0]["iosLink"];
              $scope.longitude = data[0]["longitude"];
              $scope.latitude = data[0]["latitude"];
          } else if (data[0]["status"] == "403"){
              $scope.showErrorScreen();
          }
      });

      $scope.saveform = function() {
          var passwordOld = encodeURIComponent($scope.user.passwordOld);
          var passwordNew = encodeURIComponent($scope.user.passwordNew);
          var passwordRepeat = encodeURIComponent($scope.user.passwordRepeat);
          var address = $rootScope.domainAddress + "/app-api/updateaccount/?key="+window.localStorage.getItem("AppLoginKey")+"&old="+ passwordOld + "&new=" + passwordNew + "&repeat=" + passwordRepeat;
          $http.get(address).success(function(data) {
              if(data[0]["status"] == "200"){
                  $scope.showAndHide();
                  $(".status-message").css("color", "green");
                  $scope.user.passwordOld = "";
                  $scope.user.passwordNew = "";
                  $scope.user.passwordRepeat = "";
                  $scope.savestatus = "You have successfully saved your new password";
                  window.localStorage.setItem("AppLoginKey", data[0]["AppLoginKey"]);
                  $scope.clearStatus();
              } else if (data[0]["status"] == "403"){
                  $scope.showErrorScreen();
              } else {
                  $scope.showAndHide();
                  $scope.user.passwordOld = "";
                  $scope.user.passwordNew = "";
                  $scope.user.passwordRepeat = "";
                  $scope.savestatus = data[0]["Message"];
                  $(".status-message").css("color", "red");
                  $scope.clearStatus();
              }
          });
      };

      //---------------------------------------------
      //  Redirecting methods
      //---------------------------------------------

      $scope.showErrorScreen = function () {
          $location.path('/errorpage');
      };

      $scope.clearStatus = function() {
          $timeout(clearStatus, 5000);
          function clearStatus() {
              $scope.savestatus = "";
          }
      }

      $scope.RateApp = function(){
          if (getMobileOperatingSystem()=="iOS") {
              window.open($scope.iosLink);
          } else {
              window.open($scope.androidLink, '_system');
          }
      }

      $scope.OpenMaps = function(){
          if (getMobileOperatingSystem()=="iOS") {
              var iosMapLink = "maps://?q=" + $scope.latitude + "," + $scope.longitude;
              window.open(iosMapLink);
          } else {
              var androidMapLink = "geo:" + $scope.latitude + "," + $scope.longitude + "?q=" + $scope.latitude + "," + $scope.longitude;
              window.open(androidMapLink, '_system');
          }
      }

      //$scope.openNewWindow = function() {
      //    $window.open('http://www.coach2coach.com/');
      //};

      $scope.logout = function() {
          var userEmail = window.localStorage.getItem("Email");
          localStorage.clear();

          window.localStorage.setItem("Email", userEmail);
          $location.path('/startingpage');
      };

      //---------------------------------------------
      //  Styling handlers for changing views
      //---------------------------------------------

      $scope.showAndHide = function() {
          if ($scope.changepassword=="hide"){
              $scope.changepassword = "show";
              $scope.showoptions = "show";
          }else{
              $scope.changepassword = "hide";
              $scope.showoptions = "hide";
          }
      };
      $scope.showform = function() {
          $scope.showAndHide();
      };
      $scope.showPopup = function() {
          $('#popup').show();
      };
      $scope.hidePopup = function() {
          $('#popup').hide();
      };

      function getMobileOperatingSystem() {
          var userAgent = navigator.userAgent || navigator.vendor || window.opera;
          if( userAgent.match( /iPad/i ) || userAgent.match( /iPhone/i ) || userAgent.match( /iPod/i ) ) {
              return 'iOS';
          } else {
              return 'Android';
          }
      }

      //---------------------------------------------
      //  Android fix: Fix styling of height
      //  Android fix: View from top
      //---------------------------------------------

      var newHeight = $(window).height();
      $("#profile").css("min-height", newHeight);
      $(this).scrollTop(0);

      //---------------------------------------------
      //  Transition handler method
      //---------------------------------------------

      var back = $routeParams.back;
      if (back){
          $("#profile").removeClass("fadeInUp").addClass("animated fadeInLeft");
          $("#profile .content-container").removeClass("bounceInUp");
      }
});
