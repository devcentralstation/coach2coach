myApp.controller('RegistrationController',
    function ($scope, $location, $http, $rootScope) {
        //---------------------------------------------
        //  Default values
        //---------------------------------------------

        $scope.message = "Sorry, email address or password incorrect";
        var userEmail = window.localStorage.getItem("Email");
        $scope.user = {};
        if ( userEmail != null && userEmail.length > 0){
            $scope.user.email = userEmail;
        }

        //---------------------------------------------
        //  Logging in
        //---------------------------------------------

        $scope.login = function () {
            $('#load').show();
            var email = encodeURIComponent($scope.user.email);
            var password = encodeURIComponent($scope.user.password);
            var address =  $rootScope.domainAddress + "/app-api/login/?email=" + email + "&password=" + password;
            $http.get(address).success(function (data, status, headers, config) {
                if (data["status"] == 200){
                    var rememberValue = $scope.RememberBoxChecked();
                    window.localStorage.setItem("AppLoginKey", data["AppLoginKey"]);
                    window.localStorage.setItem("FirstName", data["FirstName"]);
                    window.localStorage.setItem("Surname", data["Surname"]);
                    window.localStorage.setItem("Email",  data["Email"]);
                    window.localStorage.setItem("RememberLogin",  rememberValue);
                    $('#load').hide();
                    $location.path('/mainmenu');
                } else if (data[0]["status"] == 401){
                    $('#load').hide();
                    $scope.message = "Login details was provided was incorrect, please try again";
                    $scope.DisplayError()
                } else if (data[0]["status"] == 400){
                    $('#load').hide();
                    $scope.message = "No login details was provided, please try again";
                    $scope.DisplayError()
                } else {
                    $('#load').hide();
                    $scope.message = "A error has occurred, please try again";
                    $scope.DisplayError()
                }
            }).
            error(function(data, status, headers, config) {
                $scope.message = "A error has occurred, please try again";
                $scope.DisplayError();
                $('#load').hide();
            });
        }

        $scope.ToggleCheckBox = function(){
            $("#remember-box").toggleClass("checked");
        }

        $scope.RememberBoxChecked = function(){
            if ($("#remember-box").hasClass("checked")){
                return true;
            } else {
                return false;
            }
        }

        //---------------------------------------------
        //  Register
        //---------------------------------------------
        $scope.register = function () {
            $('#load').show();
            var email = encodeURIComponent($scope.user.email);
            var password = encodeURIComponent($scope.user.password);
            var firstname = encodeURIComponent($scope.user.firstname);
            var lastname = encodeURIComponent($scope.user.lastname);
            var address =  $rootScope.domainAddress + "/app-api/register/?email=" + email + "&password=" + password + "&firstname=" + firstname + "&surname=" + lastname;
            $http.get(address).success(function (data) {
                if (data["status"] == 200){
                    $('#load').hide();
                    window.localStorage .setItem("AppLoginKey", data["AppLoginKey"]);
                    window.localStorage .setItem("FirstName", data["FirstName"]);
                    window.localStorage .setItem("Surname", data["Surname"]);
                    window.localStorage .setItem("Email",  data["Email"]);
                    $("#register .content-container").css("display", "none");
                    $("#register-success").css("display", "block");
                } else if (data[0]["status"] == 401){
                    $('#load').hide();
                    $("#register .content-container").css("display", "none");
                    $("#register-exists").css("display", "block");
                } else {
                    $('#load').hide();
                    $("#register .content-container").css("display", "none");
                    $("#register-error").css("display", "block");
                }
            }).
                error(function() {
                    $('#load').hide();
                    $("#register .content-container").css("display", "none");
                    $("#register-error").css("display", "block");
                });
        }

        //---------------------------------------------
        //  Redirecting methods
        //---------------------------------------------

        $scope.newpassword = function () {

            var email = encodeURIComponent($scope.user.emailresubmit);
            var address =  $rootScope.domainAddress + "/app-api/forgotpassword/?email=" + email;
            $http.get(address).success(function (data) {
                $location.path('/loginforgotsuccess');
            }).
            error(function() {
                $location.path('/loginforgotsuccess');
            });
        }
        $scope.logout = function () {
            localStorage.clear();
            $location.path('/startingpage');
        }

        //---------------------------------------------
        //  Window popup handling methods
        //---------------------------------------------

        $scope.DisplayError = function () {
            $("#login .content-container").css("display", "none");
            $("#login-error").css("display", "block");
        }
        $scope.TryAgain = function () {
            $("#login .content-container").css("display", "block");
            $("#login-error").css("display", "none");
            $("#register .content-container").css("display", "block");
            $("#register-error").css("display", "none");
            $("#register-exists").css("display", "none");
        }

    });
