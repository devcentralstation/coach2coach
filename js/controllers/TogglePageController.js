myApp.controller('TogglePageController',

//    Get the JSON for the scenarios
  function($scope, $location, $http, $rootScope, $routeParams) {

      //---------------------------------------------
      //  Check for permission
      //---------------------------------------------

      var email = window.localStorage.getItem("Email");
      var appLoginKey = window.localStorage.getItem("AppLoginKey");
      var address =  $rootScope.domainAddress + "/app-api/verify/?email=" + email + "&appLoginKey=" + appLoginKey + "&page=Journals";
      $http.get(address).success(function (data) {
          if (data["status"] == 403){
              $location.path('/denied');
          }
      })

      //---------------------------------------------
      //  Default values
      //---------------------------------------------

      $scope.urlSegment = $routeParams.urlsegment;
      var toggleSection1 = $("#toggle1-section");
      var toggleSection2 = $("#toggle2-section");
      var toggleButton1 = $("#toggle1-button");
      var toggleButton2= $("#toggle2-button");

      //---------------------------------------------
      //  Retrieve Content Data
      //---------------------------------------------

      var address = $rootScope.domainAddress + "/app-api/content/" + $scope.urlSegment + "/?key="+window.localStorage.getItem("AppLoginKey");
      $http.get(address).success(function(data) {
          $("#load").hide();
          $scope.toggleItems1 = data[0]["toggle1"];
          $scope.toggleItems2 = data[0]["toggle2"];
          $scope.pagetitle =  data[0]["pagetitle"];
          $scope.blueboxtext =  data[0]["blueboxtext"];
          $scope.bannerimage =  data[0]["bannerimage"];
          $scope.toggleButton1 =  data[0]["button1"];
          $scope.toggleButton2 =  data[0]["button2"];
          $("#dosdonts").removeClass("scroll-bounce");
          setTimeout(function(){$("#dosdonts").addClass("scroll-bounce");},1000);
          if (data[0]["status"] == "403"){
              $("#load").hide();
              $scope.showErrorScreen();
          }
      }).
          error(function(data, status, headers, config) {
              $("#load").hide();
              $scope.showErrorScreen()
          });

      //---------------------------------------------
      //  Redirecting methods
      //---------------------------------------------

      $scope.showErrorScreen = function(){
          $location.path('/errorpage');
      };

      //---------------------------------------------
      //  Toggling methods
      //---------------------------------------------

      $scope.toggleText = function(pointid, togglesection) {
          var moreTextID = $("#toggle"+ togglesection + "-text" + pointid);
          var itemID = moreTextID.closest('.menu-item');
          var hasBeenToggled = moreTextID.parent().hasClass("toggle");
          $(".description ").each(function(){
              $(this).slideUp();
          });
          $(".toggle ").each(function(){
              $(this).removeClass("toggle");
          });
          if (hasBeenToggled){
              moreTextID.parent().removeClass("toggle");
              moreTextID.slideUp();
          } else {
              moreTextID.parent().addClass("toggle");
              moreTextID.slideDown(function(){
                  $('html, body').animate({
                      scrollTop: itemID.offset().top - 50
                  }, 500);
              });
          }
      };

      $scope.showToggleSection1 = function(){
          toggleButton1.addClass("selected");
          toggleButton2.removeClass("selected");
          $("#toggle1-section").removeClass("hidden");
          $("#toggle2-section").addClass("hidden");
      };
      $scope.showToggleSection2 = function(){
          toggleButton2.addClass("selected");
          toggleButton1.removeClass("selected");
          $("#toggle1-section").addClass("hidden");
          $("#toggle2-section").removeClass("hidden");
      };


});
