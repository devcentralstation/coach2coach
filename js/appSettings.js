/*
  This file comprises of setting changes on devices
 */

//Disable back button for android
document.addEventListener("deviceready", onDeviceReady, false);
function onDeviceReady() {
    document.addEventListener("backbutton", function (e) {
        e.preventDefault();
    }, false );
}