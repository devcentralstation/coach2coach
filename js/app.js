var myApp = angular.module('myApp', [
    'ngRoute',
    'ngSanitize',
    'appControllers',
    'angular-carousel']);
var appControllers = angular.module('appControllers', []);
myApp.run(function ($rootScope, $sce) {
    $rootScope.domainAddress = 'http://www.sigmoidcurve.com/';
    $rootScope.htmlSafe = function(htmlContent){
        return $sce.trustAsHtml(htmlContent);
    }
});

//$(document).ready(function () {
//    $(".text-content").delegate( "a", "click", function() {
//        console.log('clicked');
//    });
//});

myApp.config(['$routeProvider', function ($routeProvider) {
    $routeProvider.
        //Login Routes
        when('/login', {
            templateUrl: 'views/login/login.html',
            controller: 'RegistrationController'
        }).
        when('/loginforgotsuccess', {
            templateUrl: 'views/login/login-forgotsuccess.html',
            controller: 'RegistrationController'
        }).
        when('/lostpassword', {
            templateUrl: 'views/login/login-forgot.html',
            controller: 'RegistrationController'
        }).

        //Explore Routes
        when('/explore', {
            templateUrl: 'views/register/explore.html',
            controller: 'ExploreController'
        }).
        when('/denied', {
            templateUrl: 'views/register/denied.html'
        }).
        when('/deniedgameplan', {
            templateUrl: 'views/register/deniedgameplan.html'
        }).
        when('/deniedscenario', {
            templateUrl: 'views/register/deniedscenario.html'
        }).

        //Register Routes
        when('/register', {
            templateUrl: 'views/register/register.html',
            controller: 'RegistrationController'
        }).
        when('/termsconditions', {
            templateUrl: 'views/register/termsconditions.html',
            controller: 'TermsController'
        }).
        when('/termsconditionsabout', {
            templateUrl: 'views/register/termsconditionsabout.html',
            controller: 'TermsController'
        }).
        when('/registerwelcome', {
            templateUrl: 'views/register/register-welcome.html',
            controller: 'RegistrationController'
        }).

        //App Pages Routes
        when('/mainmenu', {
            templateUrl: 'views/navigation/mainmenu.html',
            controller: 'MainMenuController'
        }).
        when('/profile', {
            templateUrl: 'views/profile/profile.html',
            controller: 'ProfileController'
        }).
        when('/haroldbio', {
            templateUrl: 'views/profile/haroldbio.html',
            controller: 'BioController'
        }).

        //Pages
        when('/content/content/:urlsegment', {
            templateUrl: 'views/pages/SinglePageTemplate.html',
            controller: 'SinglePageController'
        }).
        when('/content/list/:urlsegment', {
            templateUrl: 'views/pages/ListPageTemplate.html',
            controller: 'ListController'
        }).
        when('/content/list/:urlsegment/:listItemID', {
            templateUrl: 'views/pages/ListTemplate.html',
            controller: 'ListDetailController'
        }).
        when('/content/accordian/:urlsegment', {
            templateUrl: 'views/pages/AccordianPageTemplate.html',
            controller: 'AccordianPageController'
        }).
        when('/content/toggle/:urlsegment', {
            templateUrl: 'views/pages/ToggleTemplate.html',
            controller: 'TogglePageController'
        }).
        when('/journals', {
            templateUrl: 'views/journal/journals.html',
            controller: 'JournalController'
        }).
        when('/journals/:journalID', {
            templateUrl: 'views/journal/journal.html',
            controller: 'JournalDetailController'
        }).
        when('/startingpage', {
            templateUrl: 'views/startingpage.html',
            controller: 'StartPageController'
        }).
        when('/errorpage', {
            templateUrl: 'views/pages/error.html'
        }).
        otherwise({
            templateUrl: 'views/startingpage.html',
            controller: 'StartPageController'
        });
}]);
